# A repository for PPW Stories

Introduction to Test Driven Development, Selenium testing, Java Script & JQuery, Ajax implementation - Web Design and Programming @ University of Indonesia 2018/2019

[![coverage report](https://gitlab.com/Nisrinous/ppwstory6/badges/master/coverage.svg)](https://gitlab.com/Nisrinous/ppwstory6/commits/master)  [![pipeline status](https://gitlab.com/Nisrinous/ppwstory6/badges/master/pipeline.svg)](https://gitlab.com/Nisrinous/ppwstory6/commits/master)
* * *

<ul>
<li>Link heroku : <a href="http://tempatcurahanhati.herokuapp.com/">tempatcurahanhati.herokuapp.com</a></li>
<li>Name        : Nur Nisrina Ningrum</li>
<li>NPM         : 1706979423</li>
<li>Class       : PPW A</li>
</ul>
