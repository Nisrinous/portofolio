$(document).ready(function() {
  var check = 0;
  $(".change-theme").click(function(){
    if (check == 1){
      pink(); check = 0;
    } else {
      blue(); check = 1;
    }
  })

  function pink() {
    $('body').css('background-color' , 'white')
    $('.container-fluid').css({
      'background-color' : 'white'
    })
    $('.topnav').css({
      'background-color' : 'white',
      'color' : '#BC8F8F'
    })
    $('.active').css({
      'background-color' : 'white',
      'color' : '#BC8F8F'
    })
    $('.accordion').css({
      'background-color' : 'white',
      'color' : '#BC8F8F'
    })
    $('.panel').css({
      'background-color' : 'white'
    })
    // $('.topnav a').addClass('hover').css({
    //   'background-color' : '#ECDCEF',
    //   'color' : '#BC8F8F'
    // })
    // $('.topnav a').addClass('active').css({
    //   'background-color' : 'white'
    // })
    $('.my-name').css({
      'color' : '#D2B48C'
    })
    $('.active').addClass('accordion').css({
      'background-color' : 'pink'
    })
    $('.name-year-etc').css({
      'color' : '#CCAD9D'
    })
    $('.string').css({
      'color' : '#826C61'
    })
  }

  function blue() {
    $('body').css('background-color', 'black')
    $('.container-fluid').css({
      'background-color' : 'black'
    })
    $('.topnav').css({
      'background-color' : 'black',
      'color' : '#8FBCBC'
    })
    $('.active').css({
      'background-color' : 'black',
      'color' : '#8FBCBC'
    })
    $('.accordion').css({
      'background-color' : 'black',
      'color' : '#8FBCBC'
    })
    $('.panel').css({
      'background-color' : 'black'
    })
    // $('.topnav a').addClass('hover').css({
    //   'background-color' : '#DFEFDC',
    //   'color' : '#8FBCBC'
    // })
    // $('.topnav a').addClass('active').css({
    //   'background-color' : 'black'
    // })
    $('.my-name').css({
      'color' : '#8CAAD2'
    })
    $('.active').addClass('accordion').css({
      'background-color' : '#C0FFF4'
    })
    $('.name-year-etc').css({
      'color' : '#9DBCCC'
    })
    $('.string').css({
      'color' : '#617782'
    })
  }

  $(".accordion").click(function() {
    $(this).toggleClass("active").each(function() {
      var panel = $(this).next();
      if (panel.css('max-height') == '100px') {
        panel.css('max-height', '');
      } else {
        panel.css('max-height', '100px');;
      }
    })
  })
});
