from django.apps import AppConfig


class NursdiaryappConfig(AppConfig):
    name = 'nursDiaryapp'
