from django.db import models

class Diary(models.Model):

    diary = models.CharField(max_length=300)
    diary_created = models.DateTimeField(auto_now=True)

class Register(models.Model):

    email = models.EmailField()
    namefield = models.CharField(max_length=40)
    password = models.CharField(max_length=40)
