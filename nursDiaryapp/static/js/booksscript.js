$(document).ready(function(){
  $("#str-to-search").bind('keypress', function(e) {
    if (e.keyCode == 13) {
      var searchword = $("#str-to-search").val();
      if (searchword === "") {
        alert("Please fill the search box to find books");
        return;
      }
      $.ajax({
        url: "/books/" + searchword,
        dataType: "json",
        success: function(response){
          search(response);
        }
      })
    }
  })
  $("#search-btn").click(function() {
    var searchword = $("#str-to-search").val();
    if (searchword === "") {
      alert("Please fill the search box to find books");
      return;
    }
    $.ajax({
      url: "/books/" + searchword,
      dataType: "json",
      success: function(response){
        search(response);
      }
    })
  })
  $.ajax({
    url: "/books/cat",
    dataType: "json",
    success: function(response) {
      search(response);
    }
  })
})

var numFav = 0;
function star(id) {
  var starPic = $('#'+id).attr('src');
  if (starPic.includes("https://img.icons8.com/plasticine/2x/star.png")) {
    $('#'+id).attr('src',"https://img.icons8.com/carbon-copy/2x/star.png");
    numFav--;
    $('.string-numfav').html("<center>" + numFav + "</center>");
  } else {
    $('#'+id).attr('src',"https://img.icons8.com/plasticine/2x/star.png");
    numFav++;
    $('.string-numfav').html("<center>" + numFav + "</center>");
  }
}


function search(response) {
  var allData = "";
  response = response["items"];
  for (var i = 0; i < response.length; i++) {
    var title = response[i]["volumeInfo"]["title"];
    var publisher = response[i]["volumeInfo"]["publisher"];
    var publishedDate = response[i]["volumeInfo"]["publishedDate"]
    var img = response[i]["volumeInfo"]["imageLinks"]["thumbnail"];
    var imgsrc = "<img src='"+img+"'>";
    var gambar = response[i]["id"];
    var yellowBlueStar = "<img onclick='star("+"\""+gambar+"\""+")' id='"+gambar+"' class='yellowstar-image' src='https://img.icons8.com/carbon-copy/2x/star.png'>";

    allData += "<tr><td>"+ title +
               "</td><td>"+ publisher +
               "</td><td><center>"+ publishedDate +
               "</center></td><td><center>"+ imgsrc +
               "</center></td><td><center>"+ yellowBlueStar +
               "</center></td></tr>";
  }
  $('#booksTable').append(allData);
}
