from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .models import *
from .forms import *
from datetime import datetime
import requests
import json

def diary(request):
    diary_form = DiaryForm()
    diary_message = Diary.objects.all().order_by('-diary_created')

    if request.method == 'POST':
        diary_form = DiaryForm(request.POST)
        if diary_form.is_valid():
            diary_form.save()
            return HttpResponseRedirect("/diary/")
    return render(request, 'landingpage.html', {'diary_message': diary_message, 'diary_form': diary_form})


def profile(request):
    return render(request, 'profilepage.html')

def delete(request):
    if request.method == 'POST':
        deleting = Diary.objects.all().delete()

    return HttpResponseRedirect("/diary/")

def books(request):
    return render(request, 'bookpage.html')

def get_books(request, judul):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + judul
    data = requests.get(url)
    datajson = data.json()

    return JsonResponse(datajson)

def register(request):
    form = RegisterForm(request.POST)
    return render(request, 'registerpage.html', {'register_form': form})

def register_form(request):
    if (request.method == 'POST'):
        form = RegisterForm(request.POST)
        print('bawah post')
        if form.is_valid():
            name = request.POST['namefield']
            email = request.POST['email']
            pw = request.POST['password']
            Register.objects.create(namefield = name, email = email, password = pw)

            return HttpResponseRedirect('/')

    email = request.GET.get('email', '')
    data = {
        'is_taken': False
    }
    if (Register.objects.filter(email = email).exists()):
        data['is_taken'] = True

    return JsonResponse(data)
